package com.esgi.irl.infrared.love.demo.models;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Builder
@ToString
public class UserModel {
    @Getter
    @Id
    private String userId;

    @Getter
    private String email;

    @Getter
    private String fullname;
}
