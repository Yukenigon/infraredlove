package com.esgi.irl.infrared.love.demo.repository;

import com.esgi.irl.infrared.love.demo.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<UserModel, String> {
    UserModel findByUserId(String userId);
}
