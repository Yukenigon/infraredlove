package com.esgi.irl.infrared.love.demo.controllers;

import com.esgi.irl.infrared.love.demo.models.UserModel;
import com.esgi.irl.infrared.love.demo.repository.UserRepository;
import com.esgi.irl.infrared.love.demo.utils.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class IRLController {
    private final
    UserRepository userRepository;

    private final
    SendMail sendMail;

    @Autowired
    public IRLController(UserRepository userRepository, SendMail sendMail) {
        this.userRepository = userRepository;
        this.sendMail = sendMail;
    }

    @RequestMapping(value = "/user/{id}/{fullname}/{email}", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@PathVariable("id") String id, @PathVariable("fullname") String fullname ,@PathVariable("email") String email){
        userRepository.insert(UserModel.builder().userId(id).email(email).fullname(fullname).build());
        return new ResponseEntity<>(userRepository.findByUserId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(@PathVariable("id") String id){
        return new ResponseEntity<>(userRepository.findByUserId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/mail/{sender}/{receiver}", method = RequestMethod.GET)
    public void sendMail(@PathVariable("sender") String sender, @PathVariable("receiver") String receiver){
        sendMail.sendMail(sender, receiver);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<?> getAllUsers(){
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") String id){
        userRepository.deleteById(id);
        return new ResponseEntity<>(userRepository.findByUserId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteAll(){
        userRepository.deleteAll();
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }
}
