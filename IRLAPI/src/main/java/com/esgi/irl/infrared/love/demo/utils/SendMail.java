package com.esgi.irl.infrared.love.demo.utils;

import com.esgi.irl.infrared.love.demo.models.UserModel;
import com.esgi.irl.infrared.love.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Component
public class SendMail {
    private final
    UserRepository userRepository;

    @Autowired
    public SendMail(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void sendMail(String _sender, String _receiver){
        final String username = "timothee.arnauld1@gmail.com";
        final String password = "$YwtDzbszW9lXL";

        UserModel sender = userRepository.findById(_sender).get();
        UserModel receiver = userRepository.findById(_receiver).get();

        String emailSender = sender.getEmail();
        String emailReceiver = receiver.getEmail();

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(emailReceiver));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailReceiver));
            message.setSubject("New match!");
            message.setText("Dear Mail Crawler,"
                    + "\n\n You have received a match from :" + emailSender);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
