package com.example.kamran.bluewhite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class signup extends AppCompatActivity {
    Button sin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        sin = (Button)findViewById(R.id.sin);
        sin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String fullname = ((EditText)findViewById(R.id.fname)).getText().toString();
                String code = ((EditText)findViewById(R.id.usrusr)).getText().toString();
                String email = ((EditText)findViewById(R.id.mail)).getText().toString();
                createAccount(fullname, email, code);
            }
        });
    }

    private void createAccount(String fullname, String email, String code){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://172.20.10.8:8080/api/user/" + code + "/" + fullname + "/" +  email;
        Log.d("URL:", url);
// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Toast.makeText(getApplicationContext(), "Your account is created", Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "A problem occured, please try later...", Toast.LENGTH_SHORT).show();
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
