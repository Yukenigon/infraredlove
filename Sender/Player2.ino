/* Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 * 
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Create a WiFi access point and provide a web server on it. */

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the &onfiguration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ESP8266mDNS.h>  
#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRsend.h>
#include "UbidotsMicroESP8266.h"

#define TOKEN  "A1E-7A1Tj7vomSiFXVM6gLSAjYPvGlPoqc"  // Put here your Ubidots TOKEN
#define PLAYER_1 "5ad8626bc03f9705705e3788" // Put your variable ID here
#define PLAYER_2 "5ad86!2b2c03f9705727131c5" // Put your variable ID here
#define WIFISSID "iPhone de Yous" // Put here your Wi-Fi SSID
#define PASSWORD "blablabla" // Put here your Wi-Fi password


/* Set these to your desired credentials. */
int cpt_user1 = 0;
int cpt_user2 = 0;
const int receiver = D6;
const int buttonPin = D5;
const int ledPin =  D7;
int buzzerPin = D2;
uint16_t rawData[30] = {2345, 2345, 2345, 2345,2345, 2345, 2345, 2345, 2345, 2345,2345, 2345, 2345, 2345,2345, 2345, 2345, 2345, 2345, 2345,2345, 2345, 2345, 2345,2345, 2345, 2345, 2345, 2345, 2345};

int buttonState = 0;

Ubidots client(TOKEN);


IRrecv irrecv(receiver);
IRsend irsend(ledPin);
decode_results results;
int isLocked = 0;
void buzzer(){
  digitalWrite(buzzerPin, HIGH);
  delay(1000);           
  digitalWrite(buzzerPin, LOW);
}

void turnOnLed(){
 for(int i=0;i<6;i++){
  // irsend.sendRaw(rawData,10, 38);
     irsend.sendNEC(0xFA3FC75F, 32);
   delay(40);
 }
}



void setup() {
  delay(1000);
  Serial.begin(9600);
  irrecv.enableIRIn();
  pinMode(buzzerPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  Serial.println();
  Serial.print("Configuring access point...");
  
  client.wifiConnection(WIFISSID, PASSWORD);
  Serial.println("Connexion server started");
}

void loop() {
  buttonState = digitalRead(buttonPin);
  
  if (irrecv.decode(&results)) { 
    Serial.printf("%4X\n", results.value);
    if(results.value == 0xE588C19A){
      buzzer(); 
      cpt_user2++;
      client.add(PLAYER_2, cpt_user2);
      client.sendAll(true);
    }
    irrecv.resume();
  }

  if(buttonState == LOW && isLocked == 0){
      isLocked = 1;
      turnOnLed();
  }

  if(buttonState == HIGH){
    isLocked = 0;
  }
}
