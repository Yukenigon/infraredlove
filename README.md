
# README #

![Logo](https://bytebucket.org/Yukenigon/infraredlove/raw/776e7a67fe28f73596df7f457fa23a91ae02187c/img/logo.png)

> InfraRedLove

This README would normally document whatever steps are necessary to get your application up and running.

### Who are we?

We are four students, code lovers and motivated.
We started this project for school and share it with people who are interested by. 

### What is this repository for? 

This project is entirely free for every usages. You can build it for fun or commercial use, whatever you want.

InfraredLove is a solution which makes matching between persons.
InfraredLover goal is to be a smartwatch that emit a signed infrared to an other user.
This will make a bip and send him an sms with the phone number.

### How do I get set up? 

The project is composed by 4 subprojects:

- **IRLAPI:** A restful API allowing to create and manage users accounts, send emails.
- **IRLApp:** A mobile App which allows the user to create and follow his notifications.
- **Sender:** An infrared send sending a signal to a remote controller.
- **Receiver:** An infrared sensor receiving a signal and sending a sms to the owner.

The code of the watch is in the directoryies Sender and Receiver.
That is the same code, just the signature of signal is changed.

### Installation:

The Restful API is writing with Springboot and Java 8. You need to have Gradle installed. Just import it in your favourite IDE (Intellij or Eclipse for instance).
You need a mongoDB docker container which you can deploy using the command line:

    docker run -p 27017:27017 --name mongoirlapi -d mongo

The best practice is to deploy it as a Docker container exposing the 8080 port which is used by default in our version.
This is an example of a dockerfile that you can use:

    FROM ubuntu:15.04
    RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common && \
    add-apt-repository ppa:webupd8team/java -y && \
    apt-get update && \
    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    apt-get install -y oracle-java8-installer git && \
    apt-get clean
    RUN cd ~
    RUN git clone https://Yukenigon@bitbucket.org/Yukenigon/infraredlove.git infraredlove
    RUN cd infraredlove/IRLAPI
    RUN mvn -N io.takari:maven:wrapper
    RUN java -jar target/EXECUTABLE_NAME

To setup the android app, the only thing you need to do is to download Android Studio and import sources as an existing project.
Please consider you have to change your Rest API address at the line 40 in the file:

> IRLApp/app/src/main/java/com/kamran/bluewhite/signup.java

The target SDK is set on 23 but you can change it as you want depending on the android version you use.

The build application looks like this:

![enter image description here](https://bytebucket.org/Yukenigon/infraredlove/raw/a3aee93ed8b61485affbdc723f9c7a754ec2a496/img/Screenshot_20180419-112816.png)

![enter image description here](https://bytebucket.org/Yukenigon/infraredlove/raw/a3aee93ed8b61485affbdc723f9c7a754ec2a496/img/Screenshot_20180419-112821.png)

### Furniture :

- 1 buzzer: Approximately 80cts https://tinyurl.com/yb9gajld
- 1 IR transmittor: https://tinyurl.com/ydfnslkb
- 1 IR sensor: https://tinyurl.com/ybfjyyb9  Be careful and refer to the documentation of your sensor to make sure that you plugged correctly.
- 1 button: https://tinyurl.com/yd2weeae
- 1 ESP8266: https://tinyurl.com/ydbr77ke The main core of our module.
- Breadboard: To connect everything: https://tinyurl.com/y78ksatt

You have to build everything from scratch. To achieve our module, we used a breadboard and connect sensors, LEDs and buttons.

This is how the prototype looked like at the end of the week:

![enter image description here](https://bytebucket.org/Yukenigon/infraredlove/raw/a3aee93ed8b61485affbdc723f9c7a754ec2a496/img/Image%20uploaded%20from%20iOS.jpg)

![enter image description here](https://bytebucket.org/Yukenigon/infraredlove/raw/9d91428ac84caa744ddf8ea2604e40a83ec410de/img/index.jpeg)

We drew a schema to make it more clear:

![enter image description here](https://bytebucket.org/Yukenigon/infraredlove/raw/776e7a67fe28f73596df7f457fa23a91ae02187c/img/schema.png)

You can build it within less than an hour, it is quite simple but the only thing that you should know is that you have to be careful and change the pins' name you use in the code (receiver and sender). 

### Dependencies : 

You will need a dependency to Ubidots library (ubidotsMicroESP8266) in order to register the match
ESP8266WiFi for the WiFi connection
IRremoteESP8266 for the IR management

### Who do I talk to? ###

 - Arnauld Timoth�e 
 - Nicolas Sirac 
 - Vartan Kokoghlanian 
 - Yousria Benchadi

